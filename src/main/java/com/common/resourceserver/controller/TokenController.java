package com.common.resourceserver.controller;

import com.common.resourceserver.dto.Token;
import com.common.resourceserver.service.TokenService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@Tag(name = "Authentication")
@RequiredArgsConstructor
@RestController
@RequestMapping("oauth")
public class TokenController {

    private final TokenService tokenService;

    @Operation(summary = "Create token")
    @PostMapping(value = "token", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public Token getToken(
            @Parameter(schema = @Schema(description = "User name", example = "admin"), required = true) String username,
            @Parameter(schema = @Schema(description = "password", example = "123456"), required = true) String password) {
        return tokenService.getToken(username, password);
    }

    @Operation(summary = "Refresh token")
    @PostMapping("token/refresh")
    public Token refreshToken(@Parameter(schema = @Schema(description = "refresh token", example = "132asd4f65asd1f2"), required = true)
                                  @RequestParam("refresh_token") String refreshToken) {
        return tokenService.refreshToken(refreshToken);
    }

    @Operation(summary = "Revoke token")
    @DeleteMapping("revoke/{token:.*}")
    public void getToken(@Parameter(schema = @Schema(description = "Token", example = "132asd4f65asd1f2")) @PathVariable("token") String token) {
        tokenService.revokeToken(token);
    }
}
