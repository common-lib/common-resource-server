package com.common.resourceserver.service;


import com.common.resourceserver.dto.Token;

public interface TokenService {

    Token getToken(String username, String password);

    void revokeToken(String token);

    Token refreshToken(String refreshToken);
}
